<?php

namespace App\Form;

use App\Entity\Square;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SquareType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'required' => false,
              ])
            ->add('subtitle', TextType::class, [
                'label' => 'Sous-titre',
                'required' => false,
              ])
            ->add('image', FileType::class, [
                'label' => 'Image',
                'mapped' => false,
                'required' => false,
            ])
            ->add('position')
            ->add('inThePipeline', ChoiceType::class, [
                'expanded' => true,
                'multiple' => false,
                'label' => 'Dans les tuyaux (sera placé plus bas, sans modale)',
                'required' => true,
                 'choices' => ['Oui' => true, 'Non' => false],
              ])
            ->add('twoWords', CKEditorType::class, [
                'label' => '2 mots à propos du projet',
                'required' => false,
              ])
            ->add('dataType', TextType::class, [
                'label' => 'Types de données',
                'required' => false,
              ])
            ->add('state', TextType::class, [
                'label' => 'Etat du projet',
                'required' => false,
              ])
            ->add('startDate', TextType::class, [
                'label' => 'Date de début',
                'required' => false,
              ])
            ->add('endDate', TextType::class, [
                'label' => 'Date de fin',
                'required' => false,
              ])
            ->add('link', TextType::class, [
                'label' => 'Lien',
                'required' => false,
              ])
            ->add('elan', CKEditorType::class, [
                'label' => 'ELAN dans ce projet',
                'required' => false,
              ])
            ->add('manager', TextType::class, [
                'label' => 'Responsable',
                'required' => false,
              ])
            ->add('color', ColorType::class, [
              'label' => 'Couleur du fond',
              'required' => true,
            ])
            ->add('textColor', ColorType::class, [
              'label' => 'Couleur du texte',
              'required' => false,
            ])
            ->add('save', SubmitType::class, array(
              'label' => 'Sauvegarder',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Square::class,
        ]);
    }
}
