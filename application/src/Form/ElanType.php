<?php

namespace App\Form;

use App\Entity\Elan;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ElanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'required' => false,
              ])
            ->add('subtitle', TextType::class, [
                'label' => 'Sous-titre',
                'required' => false,
              ])
              ->add('link', TextType::class, [
                'label' => 'Lien',
                'required' => false,
              ])
              ->add('content', CKEditorType::class, [
                  'label' => 'Contenu',
                  'required' => false,
                ])
            ->add('save', SubmitType::class, array(
              'label' => 'Sauvegarder',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Elan::class,
        ]);
    }
}
