<?php

namespace App\Command;

use App\Entity\User;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MakeadminCommand extends Command
{
    private $em;
    private $um;
    protected static $defaultName = 'app:toggleadmin';

    public function __construct(EntityManagerInterface $em, UserManager $um)
    {
        $this->um = $um;
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Switch status for a given user')
            ->addArgument('mail', InputArgument::REQUIRED, 'mail')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $mail = $input->getArgument('mail');

        if ($mail) {
            if ($user = $this->em->getRepository(User::class)->findOneByEmail($mail)) {
                $this->um->switchAdmin($user);
                $io->success("switch done");
            } else {
                $io->success('user not found');
            }
        } else {
            $io->success('need to specify a user email');
        }
    }
}
