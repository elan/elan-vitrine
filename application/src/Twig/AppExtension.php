<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('hex2rgb', [$this, 'hex2rgb']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('hex2rgb', [$this, 'hex2rgb']),
        ];
    }

    public function hex2rgb($hex, $opacity)
    {
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        
        return "$r, $g, $b, $opacity";
    }
}
