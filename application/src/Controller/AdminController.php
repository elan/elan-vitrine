<?php

namespace App\Controller;

use App\Entity\Square;
use App\Entity\Elan;
use App\Entity\User;
use App\Form\SquareType;
use App\Form\ElanType;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/export", name="export")
     */
    public function export()
    {
        $squares = $this->getDoctrine()->getRepository(Square::class)->findAll();
        $xml =  $this->renderView('square/export-all.xml.twig', ['squares' => $squares]);

        return new Response($xml, 200, [
            'Content-Type' => 'application/xml',
            'Content-Disposition' => 'attachment; filename="projects.xml"',
        ]);
    }


    /**
     * @Route("/edit/{id}", name="create_square", defaults={"id": null})
     */
    public function editSquare(Square $square = null, Request $request, EntityManagerInterface $em)
    {
        $square = ($square) ? $square : new Square;
        $form =  $this->createForm(SquareType::class, $square);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($file = $form['image']->getData()) {
                $fullFileName = $file->getClientOriginalName();
                $fileName = pathinfo($fullFileName, PATHINFO_FILENAME);
                $random = md5(uniqid());
                $directory = $this->getUploadDir();
                $extension = $file->guessExtension();
                $finalFileName = $fileName.'-'.$random.'.'.$extension;
                $file->move($directory, $finalFileName);

                $square->setImageURL($finalFileName);
            }


            $square->setLastUpdate(new \Datetime);
            $em->persist($square);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('square/form.html.twig', [
          "form" => $form->createView(),
          "square" => $square
        ]);
    }

    private function getUploadDir()
    {
        return $this->getParameter('upload_directory');
    }


    /**
     * @Route("/remove/{id}", name="remove_square")
     */
    public function removeSquare(Square $square, EntityManagerInterface $em)
    {
        $em->remove($square);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/create/elan/{id}", name="create_elan", defaults={"id": null})
     */
    public function editElan(Elan $elan = null, Request $request, EntityManagerInterface $em)
    {
        $elan = ($elan) ? $elan : new Elan;
        $form =  $this->createForm(ElanType::class, $elan);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $elan->setLastUpdate(new \Datetime);
            $em->persist($elan);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('square/form.html.twig', [
            "form" => $form->createView()
          ]);
    }

    /**
     * @Route("/users", name="list_users")
     */
    public function index()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('admin/users.html.twig', [
          "users" => $users
        ]);
    }

    /**
     * @Route("/user/switch/{id}", name="switch_admin")
     */
    public function switchUser(User $user, UserManager $um)
    {
        $um->switchAdmin($user);
        $this->addFlash('success', "switch admin done");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/user/{id}", name="delete_user")
     */
    public function deleteUser(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('success', "utilisateur supprimé");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/users/export", name="export_users")
     */
    public function exportUsers()
    {
        $encoders = [new CsvEncoder(";")];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        $fileSystem = new Filesystem();
        $root = $this->getParameter("kernel.project_dir") . "/public/";
        $file = "export/" . uniqid() . ".csv";
        $csvContent = $serializer->serialize($users, 'csv');
        $fileSystem->appendToFile($root.$file, $csvContent);
        $this->addFlash('success', "export OK");

        return $this->render('admin/users.html.twig', [
         "users" => $users,
         "file" => $file
        ]);
    }
}
