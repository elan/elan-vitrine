<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Square;
use App\Entity\Elan;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(EntityManagerInterface $em)
    {
        $elan = $em->getRepository(Elan::class)->findAll();
        $elan = ($elan) ? $elan[0] : null;

        $squares = $em->getRepository(Square::class)->findBy(["inThePipeline" => false], ["position" => "ASC"]);
        $inThePipelines = $em->getRepository(Square::class)->findBy(["inThePipeline" => true], ["position" => "ASC"]);

        return $this->render('default/index.html.twig', ["squares" => $squares, 'elan' => $elan, "inThePipelines" => $inThePipelines]);
    }

    /**
     * @Route("/mention-legales", name="legal_notices")
     */
    public function legalNotices()
    {
        return $this->render('default/legal-notices.html.twig');
    }
}
