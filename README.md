# Site ELAN.

## Installation
```
git clone
cd elan-carres
cp .env.dist .env
vi .env
sudo docker-compose up --build -d
sudo docker-compose exec apache make init
```

## Mise à jour
```
git pull origin master
sudo docker-compose exec apache make update
```


## Licence
[MIT](LICENSE)
